import { useState } from "react";
import { fetchPokemon } from "../../App"
import SearchTmp from "./SearchTmp";

export default function Search () {
  const [search, setSearch] = useState('')
  const [pokemon, setPokemon] = useState('')
  const [aSearch, setASearch] = useState(false)
  const handleButtonClick = (e) => {
    setASearch(true)
    e.preventDefault()
    fetchPokemon(search).then(
      (a) => {
        setPokemon(a)
      }
    )
  }
  const handleSearch = (e) => {
    setSearch(e)
  }
  return(
    <SearchTmp pokemon={pokemon} search={search} aSearch={aSearch} handleSearch={handleSearch} handleButtonClick={handleButtonClick} />
  )
}

