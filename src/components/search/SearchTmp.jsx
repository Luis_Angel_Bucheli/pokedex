import Card from "../card"

export default function SearchTmp ({pokemon, search, handleSearch, aSearch, handleButtonClick}) {
  return(
    <div>
      <div className="input-group">
        <input type="text" name="search" className="form-control" placeholder="Escribe el nombre de tu pokémon" value={search} onChange={(e) => handleSearch(e.target.value)}/>
        <button className="btn btn-primary" onClick={handleButtonClick}>Buscar</button>
      </div>
      <div id="container">
        { 
          pokemon? <Card { ...pokemon } /> : aSearch? <p>No hay resultados</p> : '' 
        }
      </div>
    </div>
  )
}