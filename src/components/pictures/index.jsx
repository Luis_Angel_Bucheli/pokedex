import { useState } from "react";
import { useParams } from 'react-router-dom';
import { fetchPokemon } from '../../App'
import PicturesTmp from "./PicturesTmp";

export default function Pictures () {
  const params = useParams();
  const [form, setForm] = useState('')
  fetchPokemon(params.id).then(
    (a) => {
       setForm(a?.pokeForm.sprites)
    }
  )
  let arrPict = Object.keys(form);

  return (
    <PicturesTmp form={form} arrPict={arrPict} />
  )
}