import React from "react";
export default function Picture ({ image, name }) {
  return(
    <div className="poke-picture">
      {
        image && <img src={ image } alt={ name } />
      }
    </div>
  )
}