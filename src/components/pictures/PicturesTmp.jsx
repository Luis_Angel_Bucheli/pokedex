import { Link } from "react-router-dom";
import Picture from './picture'
export default function PicturesTmp ({form, arrPict}) {
  return(
    <div>
      <div className="jumbotron">
        <h1>Pokémon Pictures</h1>
      </div>
      <Link to="/">Home</Link>
      <div className="card-columns">
        { arrPict?.map((value) => form[value] && <Picture key={ value } image={ form[value] } name={ value } /> ) }
      </div>
      <Link to="/">Home</Link>
    </div>
  )
}