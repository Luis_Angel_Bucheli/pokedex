import React from "react";
import { Link } from "react-router-dom";

export default function Card ({pokeForm, pokeInfo}) {
  const {name, height, base_experience, is_battle_only} = pokeInfo
  const {sprites} = pokeForm
  let pathForm = `/pictures/${ pokeInfo.id }`
  let pathEvol = `/evolutions/${ pokeInfo.id }` 
  return (
    <div className="card">
      <div className="card-header"><Link to={ pathForm } >{name}</Link></div>
      <div className="card-body">
        <Link to={ pathEvol } >
          <div className="poke-picture">
            <img src={sprites.front_default} alt={name} />
          </div>
        </Link>
        <ul>
          <li>height: {height}</li>
          <li>Base Exp: {base_experience}</li>
          <li>Battle Only: {is_battle_only}</li>
        </ul>
      </div>
    </div>
  )
}