import { Link } from "react-router-dom";
import Card from "../card"

export default function EvolutionsTmp ({evolutions}) {
  return (
    <div>
      <div className="jumbotron">
        <h1>Cadena de evolución</h1>
      </div>
      <Link to="/">Home</Link>
      <div className="card-columns">
      {
        evolutions.map((a, i) => { return <Card key={ i } { ...a } /> })
      }
      </div>
      <Link to="/">Home</Link>
    </div>
  )
}