import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import { fetchPokemon, getEvolutions } from '../../App'
import '../../assets/css/stylesheet.css';
import EvolutionsTmp from './EvolutionsTmp';

export default function Evolutions () {
  const params = useParams()
  const [evolutions, setEvolutions] = useState([])
  useEffect(() => {
    getEvolutions(params.id).then((evol) => {
      evol?.forEach(evolution => {
        fetchPokemon(evolution).then(
          (evolData) => {
            setEvolutions(prevEvol => {
              return [...prevEvol, evolData]
            })
          }
        )
      })
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <EvolutionsTmp evolutions={evolutions}/>
  )
}
