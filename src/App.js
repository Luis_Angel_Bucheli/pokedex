import React from "react";
import './App.css';
import Pokeroutes from "./Pokeroutes";

function App() {
  return (
    <Pokeroutes />
  );
}

export async function getPokemonDataFromUrl(url) {
  let fetchData = fetch(url)
    .then((response) => response.json())
    .then((data) => data);
  return fetchData;
}

export async function fetchPokemon(id) {
  const pokemonInfo = await getPokemonDataFromUrl(`https://pokeapi.co/api/v2/pokemon/${id}`);
  const pokemonForms = await getPokemonDataFromUrl(pokemonInfo.forms[0].url)
  const info = {
    pokeInfo: pokemonInfo,
    pokeForm: pokemonForms,
  }
  return info 
}

export function getEvolutions(id) {
  let chainArray = fetchPokemon(id).then((info) => {
    return info && getPokemonDataFromUrl(info.pokeInfo.species?.url).then((chain) => {
      return chain && getPokemonDataFromUrl(chain.evolution_chain.url).then ((dataChain) => {
        return getChainArray(dataChain.chain)
      })
    })
  })

  return chainArray
}

function getChainArray (chain) {
  let arr = []
  chain && arr.push(chain.species.name)
  chain.evolves_to?.forEach(evolves => {
    arr = arr.concat(getChainArray(evolves))
  })
  return arr
}

export default App;
