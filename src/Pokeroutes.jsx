import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Evolutions from './components/evolutions';
import Pictures from './components/pictures';
import Search from './components/search';

export default function Pokeroutes () {
  return (
    <Router>
      <div className="App container">
        <Switch>
          <Route path="/pictures/:id" >
            <Pictures />
          </Route>
          <Route path="/evolutions/:id" >
            <Evolutions />
          </Route>
          <Route path="/">
            <div className="jumbotron">
              <h1>Pokédex</h1>
            </div>
            <Search />
          </Route>
        </Switch>
      </div>
    </Router>
  )
}